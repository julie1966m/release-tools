# frozen_string_literal: true

# rubocop:disable Layout/ExtraSpacing
source "https://rubygems.org"

gem 'activesupport', '~> 4.2.0'
gem 'colorize'
gem 'dotenv',        '~> 2.2.0'
gem 'gitlab',        '~> 4.10.0'
gem 'httparty',      '~> 0.16.0'
gem 'rake'
gem 'rugged',        '~> 0.27.0'
gem 'sentry-raven',  '~> 2.7', require: false
gem 'merge_db_schema', '~> 0.1'
gem 'version_sorter', '~> 2.2.0'
gem 'parallel', '~> 1.14'

group :development, :test do
  gem 'byebug'
  gem 'climate_control', '~> 0.2.0'
  gem 'pry'
  gem 'rspec',           '~> 3.7.0'
  gem 'rspec-parameterized'
  gem 'rubocop',         '~> 0.66.0'
  gem 'rubocop-rspec',   '~> 1.32.0'
  gem 'simplecov',       '~> 0.15.0'
  gem 'timecop',         '~> 0.9.0'
  gem 'vcr',             '~> 2.9.0'
  gem 'webmock',         '~> 3.4.0'
end
# rubocop:enable Layout/ExtraSpacing
